package com.ingeneo.modelo;

import com.ingeneo.modelo.Destino;
import com.ingeneo.modelo.Pedido;
import com.ingeneo.modelo.TipoCarga;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-02-04T23:52:31")
@StaticMetamodel(Carro.class)
public class Carro_ { 

    public static volatile SingularAttribute<Carro, Date> fecha;
    public static volatile SingularAttribute<Carro, Boolean> estado;
    public static volatile SingularAttribute<Carro, BigDecimal> cargaActual;
    public static volatile SingularAttribute<Carro, TipoCarga> limiteCarga;
    public static volatile ListAttribute<Carro, Pedido> pedidoList;
    public static volatile SingularAttribute<Carro, String> conductor;
    public static volatile SingularAttribute<Carro, Destino> idDestino;
    public static volatile SingularAttribute<Carro, String> placa;

}
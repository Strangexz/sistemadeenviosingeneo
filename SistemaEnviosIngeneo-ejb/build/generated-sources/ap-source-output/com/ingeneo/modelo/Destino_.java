package com.ingeneo.modelo;

import com.ingeneo.modelo.Carro;
import com.ingeneo.modelo.Pedido;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-02-04T23:52:31")
@StaticMetamodel(Destino.class)
public class Destino_ { 

    public static volatile SingularAttribute<Destino, String> descripción;
    public static volatile ListAttribute<Destino, Carro> carroList;
    public static volatile ListAttribute<Destino, Pedido> pedidoList;
    public static volatile SingularAttribute<Destino, Integer> id;

}
package com.ingeneo.modelo;

import com.ingeneo.modelo.Carro;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-02-04T23:52:31")
@StaticMetamodel(TipoCarga.class)
public class TipoCarga_ { 

    public static volatile SingularAttribute<TipoCarga, String> descripción;
    public static volatile ListAttribute<TipoCarga, Carro> carroList;
    public static volatile SingularAttribute<TipoCarga, Integer> id;
    public static volatile SingularAttribute<TipoCarga, BigDecimal> carga;

}
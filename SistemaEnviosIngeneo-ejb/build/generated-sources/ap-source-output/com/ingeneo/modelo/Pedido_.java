package com.ingeneo.modelo;

import com.ingeneo.modelo.Carro;
import com.ingeneo.modelo.Destino;
import com.ingeneo.modelo.Usuario;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-02-04T23:52:31")
@StaticMetamodel(Pedido.class)
public class Pedido_ { 

    public static volatile SingularAttribute<Pedido, Date> fecha;
    public static volatile SingularAttribute<Pedido, Carro> placaCarro;
    public static volatile SingularAttribute<Pedido, BigDecimal> precio;
    public static volatile SingularAttribute<Pedido, String> nombreCliente;
    public static volatile SingularAttribute<Pedido, String> idCliente;
    public static volatile SingularAttribute<Pedido, BigDecimal> peso;
    public static volatile SingularAttribute<Pedido, Usuario> idUsuario;
    public static volatile SingularAttribute<Pedido, Destino> destino;
    public static volatile SingularAttribute<Pedido, Integer> idPedido;

}
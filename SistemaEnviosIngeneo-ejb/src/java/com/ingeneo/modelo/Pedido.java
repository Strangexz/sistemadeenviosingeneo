/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Strangexz
 */
@Entity
@Table(name = "pedido")
@XmlRootElement
@NamedQueries({
//    @NamedQuery(name = "Pedido.addPedido", query = "INSERT p FROM Pedido p WHERE p.fecha = :fecha AND p.destino.id = :destino AND p.placaCarro.placa = :placa"),
    @NamedQuery(name = "Pedido.findFiltro", query = "SELECT p FROM Pedido p WHERE p.fecha = :fecha AND p.destino.id = :destino AND p.placaCarro.placa = :placa"),
    @NamedQuery(name = "Pedido.findByFechaDestino", query = "SELECT p FROM Pedido p WHERE p.fecha = :fecha AND p.destino.id = :destino "),
    @NamedQuery(name = "Pedido.findByFechaPlaca", query = "SELECT p FROM Pedido p WHERE p.fecha = :fecha AND p.placaCarro.placa = :placa"),
    @NamedQuery(name = "Pedido.findByDestinoPlaca", query = "SELECT p FROM Pedido p WHERE p.destino.id = :destino AND p.placaCarro.placa = :placa"),
    @NamedQuery(name = "Pedido.findByPlaca", query = "SELECT p FROM Pedido p WHERE p.placaCarro.placa = :placa"),
    @NamedQuery(name = "Pedido.findByDestino", query = "SELECT p FROM Pedido p WHERE p.destino.id = :destino"),
    @NamedQuery(name = "Pedido.findAll", query = "SELECT p FROM Pedido p"),
    @NamedQuery(name = "Pedido.findByIdPedido", query = "SELECT p FROM Pedido p WHERE p.idPedido = :idPedido"),
    @NamedQuery(name = "Pedido.findByPrecio", query = "SELECT p FROM Pedido p WHERE p.precio = :precio"),
    @NamedQuery(name = "Pedido.findByFecha", query = "SELECT p FROM Pedido p WHERE p.fecha = :fecha"),
    @NamedQuery(name = "Pedido.findByPeso", query = "SELECT p FROM Pedido p WHERE p.peso = :peso"),
    @NamedQuery(name = "Pedido.findByNombreCliente", query = "SELECT p FROM Pedido p WHERE p.nombreCliente = :nombreCliente"),
    @NamedQuery(name = "Pedido.findByIdCliente", query = "SELECT p FROM Pedido p WHERE p.idCliente = :idCliente")})
public class Pedido implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Pedido")
    private Integer idPedido;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "Precio")
    private BigDecimal precio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Peso")
    private BigDecimal peso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Nombre_Cliente")
    private String nombreCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "Id_Cliente")
    private String idCliente;
    @JoinColumn(name = "Destino", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Destino destino;
    @JoinColumn(name = "Id_Usuario", referencedColumnName = "Username")
    @ManyToOne(optional = false)
    private Usuario idUsuario;
    @JoinColumn(name = "Placa_Carro", referencedColumnName = "Placa")
    @ManyToOne(optional = false)
    private Carro placaCarro;

    public Pedido() {
    }

    public Pedido(Integer idPedido) {
        this.idPedido = idPedido;
    }

    public Pedido(Integer idPedido, BigDecimal precio, Date fecha, BigDecimal peso, String nombreCliente, String idCliente) {
        this.idPedido = idPedido;
        this.precio = precio;
        this.fecha = fecha;
        this.peso = peso;
        this.nombreCliente = nombreCliente;
        this.idCliente = idCliente;
    }

    public Integer getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Integer idPedido) {
        this.idPedido = idPedido;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getPeso() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso = peso;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public Destino getDestino() {
        return destino;
    }

    public void setDestino(Destino destino) {
        this.destino = destino;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Carro getPlacaCarro() {
        return placaCarro;
    }

    public void setPlacaCarro(Carro placaCarro) {
        this.placaCarro = placaCarro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPedido != null ? idPedido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pedido)) {
            return false;
        }
        Pedido other = (Pedido) object;
        if ((this.idPedido == null && other.idPedido != null) || (this.idPedido != null && !this.idPedido.equals(other.idPedido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ingeneo.modelo.Pedido[ idPedido=" + idPedido + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Strangexz
 */
@Entity
@Table(name = "tipo_carga")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoCarga.findAll", query = "SELECT t FROM TipoCarga t"),
    @NamedQuery(name = "TipoCarga.findById", query = "SELECT t FROM TipoCarga t WHERE t.id = :id"),
    @NamedQuery(name = "TipoCarga.findByCarga", query = "SELECT t FROM TipoCarga t WHERE t.carga = :carga"),
    @NamedQuery(name = "TipoCarga.findByDescripci\u00f3n", query = "SELECT t FROM TipoCarga t WHERE t.descripci\u00f3n = :descripci\u00f3n")})
public class TipoCarga implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "Carga")
    private BigDecimal carga;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Descripci\u00f3n")
    private String descripción;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "limiteCarga")
    private List<Carro> carroList;

    public TipoCarga() {
    }

    public TipoCarga(Integer id) {
        this.id = id;
    }

    public TipoCarga(Integer id, BigDecimal carga, String descripción) {
        this.id = id;
        this.carga = carga;
        this.descripción = descripción;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getCarga() {
        return carga;
    }

    public void setCarga(BigDecimal carga) {
        this.carga = carga;
    }

    public String getDescripción() {
        return descripción;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    @XmlTransient
    public List<Carro> getCarroList() {
        return carroList;
    }

    public void setCarroList(List<Carro> carroList) {
        this.carroList = carroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoCarga)) {
            return false;
        }
        TipoCarga other = (TipoCarga) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ingeneo.modelo.TipoCarga[ id=" + id + " ]";
    }
    
}

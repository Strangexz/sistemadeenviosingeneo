/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Strangexz
 */
@Entity
@Table(name = "carro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Carro.findAll", query = "SELECT c FROM Carro c"),
    @NamedQuery(name = "Carro.findByPlaca", query = "SELECT c FROM Carro c WHERE c.placa = :placa"),
    @NamedQuery(name = "Carro.findByTipoCarga", query = "SELECT c FROM Carro c WHERE c.limiteCarga.id = :tipoCarga"),
    @NamedQuery(name = "Carro.findByDestino", query = "SELECT c FROM Carro c WHERE c.idDestino.id = :destino ORDER BY c.limiteCarga.carga ASC"),
    @NamedQuery(name = "Carro.findByConductor", query = "SELECT c FROM Carro c WHERE c.conductor = :conductor"),
    @NamedQuery(name = "Carro.findByCargaActual", query = "SELECT c FROM Carro c WHERE c.cargaActual = :cargaActual"),
    @NamedQuery(name = "Carro.findByFecha", query = "SELECT c FROM Carro c WHERE c.fecha = :fecha"),
    @NamedQuery(name = "Carro.findByEstado", query = "SELECT c FROM Carro c WHERE c.estado = :estado")})
public class Carro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "Placa")
    private String placa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Conductor")
    private String conductor;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "Carga_Actual")
    private BigDecimal cargaActual;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Estado")
    private boolean estado;
    @JoinColumn(name = "Id_Destino", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Destino idDestino;
    @JoinColumn(name = "Limite_Carga", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private TipoCarga limiteCarga;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "placaCarro")
    private List<Pedido> pedidoList;

    public Carro() {
    }

    public Carro(String placa) {
        this.placa = placa;
    }

    public Carro(String placa, String conductor, BigDecimal cargaActual, Date fecha, boolean estado) {
        this.placa = placa;
        this.conductor = conductor;
        this.cargaActual = cargaActual;
        this.fecha = fecha;
        this.estado = estado;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public BigDecimal getCargaActual() {
        return cargaActual;
    }

    public void setCargaActual(BigDecimal cargaActual) {
        this.cargaActual = cargaActual;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Destino getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(Destino idDestino) {
        this.idDestino = idDestino;
    }

    public TipoCarga getLimiteCarga() {
        return limiteCarga;
    }

    public void setLimiteCarga(TipoCarga limiteCarga) {
        this.limiteCarga = limiteCarga;
    }

    @XmlTransient
    public List<Pedido> getPedidoList() {
        return pedidoList;
    }

    public void setPedidoList(List<Pedido> pedidoList) {
        this.pedidoList = pedidoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (placa != null ? placa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carro)) {
            return false;
        }
        Carro other = (Carro) object;
        if ((this.placa == null && other.placa != null) || (this.placa != null && !this.placa.equals(other.placa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ingeneo.modelo.Carro[ placa=" + placa + " ]";
    }
    
}

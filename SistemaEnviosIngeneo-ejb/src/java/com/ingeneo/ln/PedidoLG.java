/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.ln;

import com.ingeneo.dao.PedidoDAO;
import com.ingeneo.modelo.Pedido;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author Strangexz
 */
@Stateless
@LocalBean
public class PedidoLG {
    @EJB
    private PedidoDAO pedidoDAO;
    
    public List<Pedido> getTodosPedidos() {
        return pedidoDAO.findAll();
    }
    
    public void crearPedido(Pedido pedido){
        pedidoDAO.create(pedido);
    }
    
    public List<Pedido> pedidosFiltrados(Date fecha, String placa, int destino){
        return pedidoDAO.listaPedidosFiltrada(fecha, placa, destino);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    
}

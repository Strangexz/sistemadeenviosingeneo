/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.ln;

import com.ingeneo.dao.UsuarioDAO;
import com.ingeneo.modelo.Usuario;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author Strangexz
 */
@Stateless
@LocalBean
public class UsuarioLG {
    @EJB
    private UsuarioDAO usuarioDAO;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    /**
     * Método para validar usuario
     * @param user String con el nombre del usuario
     * @param pass String con la contraseña del usuario
     * @return Boolean. true si existe o false en caso contrario
     */
    public boolean validarUsuario(Usuario user){
        Usuario u = usuarioDAO.find(user);
        
        if(u != null){
            return true;
        }else{
            return false;
        }
    }
    
    public int loginUsuario(String user, String pass){      
        
        return usuarioDAO.validarUser(user, pass);
    }
}

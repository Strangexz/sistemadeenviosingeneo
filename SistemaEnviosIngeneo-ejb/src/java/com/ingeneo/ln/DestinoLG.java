/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.ln;

import com.ingeneo.dao.DestinoDAO;
import com.ingeneo.modelo.Destino;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author Strangexz
 */
@Stateless
@LocalBean
public class DestinoLG {
    @EJB
    private DestinoDAO destinoDAO = new DestinoDAO();

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public List<Destino> getDestinos(){
        
        return destinoDAO.findAll();
    }
    
    public void crearDestino(Destino destino){
        destinoDAO.create(destino);
    }
}

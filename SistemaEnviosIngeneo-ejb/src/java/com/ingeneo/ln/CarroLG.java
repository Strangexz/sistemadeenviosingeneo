/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.ln;

import com.ingeneo.dao.CarroDAO;
import com.ingeneo.modelo.Carro;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author Strangexz
 */
@Stateless
@LocalBean
public class CarroLG {
    @EJB
    private CarroDAO carroDAO;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public List<Carro> getCarros(){
        return carroDAO.findAll();
    }
    
    public void createCarro(Carro carro){
        carroDAO.create(carro);
    }
    
    public List<Carro> getCarroPorCarga(int id){
       return carroDAO.getCarroPorCarga(id);
    }
    
    public List<Carro> getCarroDestino(int id){
       return carroDAO.getCarroPorDestino(id);
    }
}

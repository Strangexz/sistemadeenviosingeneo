/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.dao;

import com.ingeneo.modelo.TipoCarga;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Strangexz
 */
@Stateless
public class TipoCargaDAO extends AbstractFacade<TipoCarga> {
    @PersistenceContext(unitName = "SistemaEnviosIngeneo-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoCargaDAO() {
        super(TipoCarga.class);
    }
    
}

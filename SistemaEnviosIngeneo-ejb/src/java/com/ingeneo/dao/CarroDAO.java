/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.dao;

import com.ingeneo.modelo.Carro;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author Strangexz
 */
@Stateless
public class CarroDAO extends AbstractFacade<Carro> {
    @PersistenceContext(unitName = "SistemaEnviosIngeneo-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CarroDAO() {
        super(Carro.class);
    }
    
    public List<Carro> getCarroPorCarga(Integer tipoCarga){
        List<Carro> lista = new ArrayList<>();
       Query query;
       
       query = em.createNamedQuery("Carro.findByTipoCarga", Carro.class);
         query.setParameter("tipoCarga", tipoCarga);                 

         lista = query.getResultList();

        return lista;
    }
    
     public List<Carro> getCarroPorDestino(Integer destino){
        List<Carro> lista = new ArrayList<>();
       Query query;
       
       query = em.createNamedQuery("Carro.findByDestino", Carro.class);
         query.setParameter("destino", destino);                 

         lista = query.getResultList();

        return lista;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.dao;

import com.ingeneo.modelo.Pedido;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Strangexz
 */
@Stateless
public class PedidoDAO extends AbstractFacade<Pedido> {
    @PersistenceContext(unitName = "SistemaEnviosIngeneo-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PedidoDAO() {
        super(Pedido.class);
    }
    
    public List<Pedido> listaPedidosFiltrada(Date fecha, String placa, int destino){
        Query query;
        List<Pedido> lista = new ArrayList<>();
        
        try{
            if(fecha != null && placa != null && destino > 0){
                System.out.println("Filtro 1");
                 query = em.createNamedQuery("Pedido.findFiltro", Pedido.class);
                 query.setParameter("fecha", fecha);
                 query.setParameter("placa", placa);
                 query.setParameter("destino", destino);

                 lista = query.getResultList();
                 
            }else if(fecha != null && placa != null){
                System.out.println("Filtro 2");
                query = em.createNamedQuery("Pedido.findByFechaPlaca", Pedido.class);
                query.setParameter("fecha", fecha);
                query.setParameter("placa", placa);                 

                lista = query.getResultList();
            }else if(fecha != null && destino > 0){
                System.out.println("Filtro 3");
                query = em.createNamedQuery("Pedido.findByFechaDestino", Pedido.class);
                query.setParameter("fecha", fecha);
                query.setParameter("destino", destino); 
            }else if(destino > 0 && placa != null){
                System.out.println("Filtro 4");
                query = em.createNamedQuery("Pedido.findByDestinoPlaca", Pedido.class);                 
                query.setParameter("placa", placa);
                query.setParameter("destino", destino);

                lista = query.getResultList();
            }else if(fecha != null && placa == null && destino <= 0){
                System.out.println("Filtro 5");
                 query = em.createNamedQuery("Pedido.findByFecha", Pedido.class);
                 query.setParameter("fecha", fecha);                 

                 lista = query.getResultList();
            }else if(fecha == null && placa != null && destino <= 0){
                System.out.println("Filtro 6");
                 query = em.createNamedQuery("Pedido.findByPlaca", Pedido.class);                 
                 query.setParameter("placa", placa);                 

                 lista = query.getResultList();
            }else if(fecha == null && placa == null && destino > 0){
                System.out.println("Filtro 7");
                 query = em.createNamedQuery("Pedido.findByDestino", Pedido.class);                 
                 query.setParameter("destino", destino);

                 lista = query.getResultList();
            }else{
                query = em.createNamedQuery("Pedido.findAll", Pedido.class);
                
                lista = query.getResultList();
            }
        }catch(Exception e){
            System.out.println("ERROR: "+e.getMessage());
        }
        
        return lista;
    }
    
}

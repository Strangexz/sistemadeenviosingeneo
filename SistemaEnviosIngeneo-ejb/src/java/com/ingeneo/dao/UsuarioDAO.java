/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.dao;

import com.ingeneo.modelo.Usuario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Strangexz
 */
@Stateless
public class UsuarioDAO extends AbstractFacade<Usuario> {
    @PersistenceContext(unitName = "SistemaEnviosIngeneo-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioDAO() {
        super(Usuario.class);
    }
    
    public int validarUser(String user, String pass){
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SistemaEnviosIngeneo-ejbPU"); 
//        em = emf.createEntityManager();
        
        Query query = em.createNamedQuery("Usuario.findByUsername", Usuario.class);
        query.setParameter("username", user);       
        try{
            Usuario u = (Usuario) query.getSingleResult();
            
            if(u != null){
                if(u.getPassword().equals(pass)){
                return 1;
            }else{
                    System.out.println("No se encontro la contraseña para el usuario["+user+"]");
                return 0;
            }                
               
        }
        }catch(Exception e){
            System.out.println("ERROR: "+e.getMessage());
            return -1;
        }                
        return -1;
    }
    
}

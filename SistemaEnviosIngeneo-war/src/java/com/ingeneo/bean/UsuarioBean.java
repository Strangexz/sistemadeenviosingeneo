/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.bean;

import com.ingeneo.ln.UsuarioLG;
import com.ingeneo.modelo.Usuario;
import com.ingeneo.util.LogManager;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 *
 * @author Strangexz
 */
@ManagedBean
@SessionScoped
public class UsuarioBean implements Serializable{
    @EJB
    private UsuarioLG usuarioLogN;

    public static String nombre;
    public static String password;
    
    private static Logger log = LogManager.initLogger(UsuarioBean.class);    
    
    /**
     * Creates a new instance of UsuarioBean
     */
    public UsuarioBean() {
    }
    
    public String login(){
        log.info("Validando al usuario["+nombre+"]...");
        Usuario us = new Usuario();
        
        int result = usuarioLogN.loginUsuario(nombre, password);
        
        if(result > 0){
            log.info("Usuario validado exitosamente!!!");
//            try{
                log.info("Redireccionando...");
//                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//                ec.redirect(ec.getRequestContextPath() + "/pedidos/viewPedidos.xhtml");
//                FacesContext.getCurrentInstance().getExternalContext().redirect("./../pedidos/");
//                "/pedidos/viewPedidos.xhtml?faces-redirect=true";
                
                return "/pedidos/viewPedidos.xhtml?faces-redirect=true";
//            }catch(IOException e){
//                log.error(e.getMessage(), e);
//            }            
        }else if(result == 0){
                log.info("Contrseña incorrecta!!!");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Credenciales de acceso invalidas."));
        }else {            
            log.info("Usuario Inexistente!!!");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Usuario Inexistente."));
        }
        
        return "";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}

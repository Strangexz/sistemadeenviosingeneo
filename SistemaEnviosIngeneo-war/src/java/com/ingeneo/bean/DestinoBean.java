/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.bean;

import com.ingeneo.ln.DestinoLG;
import com.ingeneo.modelo.Destino;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Strangexz
 */
@ManagedBean
@SessionScoped
public class DestinoBean {
    @EJB
    private DestinoLG destinoService;
    private List<Destino> destinos;
    private Destino destino;

    /**
     * Creates a new instance of DestinoBean
     */
    public DestinoBean() {
    }
    
    public List<Destino> getDestinos(){
        destinos = destinoService.getDestinos();
        return destinos;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.bean;

import com.ingeneo.ln.CarroLG;
import com.ingeneo.ln.DestinoLG;
import com.ingeneo.ln.PedidoLG;
import com.ingeneo.modelo.Carro;
import com.ingeneo.modelo.Destino;
import com.ingeneo.modelo.Pedido;
import com.ingeneo.modelo.Usuario;
import com.ingeneo.util.LogManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.event.ToggleEvent;

/**
 *
 * @author Strangexz
 */
@ManagedBean
@SessionScoped
public class PedidoBean{    
    @EJB
    private DestinoLG destinoLG;
    @EJB
    private CarroLG carroLG;
    @EJB
    private PedidoLG pedidoLN;
    
    
    
    private static Logger log = LogManager.initLogger(PedidoBean.class);    
    
    private List<Pedido> pedidos;
    private List<Carro> carros;
    private List<Carro> carrosPorCarga;
    private List<Carro> carrosPorDestino;
    private List<Destino> destinos;
    
    private List<String> listaDestinos;
    private List<String> listaCarros;
    private Pedido pedido = new Pedido();
    private Pedido filtroPedido;
    
    private String usuario;
    private String idCliente;
    private String placa;           
    private Date fechaFiltro;
    private String placaFiltro;
    private int destinoFiltro;
    private int tipoCarga;
    private int idDestino = 0;
    
    /**
     * Creates a new instance of PedidoBean
     */
    public PedidoBean() {
        destinoLG = new DestinoLG();
        carroLG =  new CarroLG();
        usuario = UsuarioBean.nombre;
        log.info("Cargando listas...");        
//        cargarListas();
    }
    
    public void cargarListas(){
        listaCarros = new ArrayList<>();
        listaDestinos = new ArrayList<>();
        
        destinos = getDestinos();
        carros = getCarros();
        
         for(Destino d:destinos){
            listaDestinos.add(d.getDescripción());
        }
        for(Carro c:carros){
            listaCarros.add(c.getPlaca());
        }
    }
    
    public String guardarPedido(){
        for(Destino d:destinos){
            if(d.getId() == idDestino){
                log.info(d);
                pedido.setDestino(d);
            }
        }
        
        log.info(placa);
        for(Carro c: carros){
            if(c.getPlaca().equals(placa)){
                log.info(c);
                pedido.setPlacaCarro(c);
            }
        }
        
        Usuario user = new Usuario();
        
        user.setUsername(UsuarioBean.nombre);
        user.setPassword(UsuarioBean.password);
        user.setPedidoList(pedidos);
        pedido.setIdUsuario(user);
        
        pedido.setIdPedido(pedidos.size()+1);
        
        pedido.setIdCliente(idCliente.replace("-", ""));
        
        
        
        log.info("Se procede a guardar el pedido["+pedido+"]");
        
        StringBuilder sb = new StringBuilder();
        sb.append("PEDIDO{")
                .append("ID="+pedido.getIdPedido()+"],")
                .append("NOMBRECLIENTE="+pedido.getNombreCliente()+"],")
                .append("IDCLIENTE="+pedido.getIdCliente()+"],")
                .append("FECHA="+pedido.getFecha()+"],")
                .append("PESO="+pedido.getPeso()+"],")
                .append("DESTINO="+pedido.getDestino().getId()+"],")
                .append("PLACA="+pedido.getPlacaCarro().getPlaca()+"],")
                .append("PRECIO="+pedido.getPrecio()+"],")
                .append("USUARIO="+pedido.getIdUsuario()+"],")                                
                .append(sb);
        
        log.info(sb);
        
        pedidoLN.crearPedido(pedido);
        pedidos = null;
        
        log.info("Pedido Guardado!!!!");
        return "/pedidos/viewPedidos.xhtml?faces-redirect=true";
    }
    
    public String prepararNuevo(){
       pedido = new Pedido();
       return "/pedidos/crearPedido.xhtml";
    }
    
    public String cancelar(){
        pedido= null;
	return "/pedidos/viewPedidos.xhtml";
    }
    
    public void buscarFiltroPedidos(){
        log.info("Filtrando pedidos...");
        pedidos = null;
        
        pedidos = pedidoLN.pedidosFiltrados(fechaFiltro, placaFiltro, destinoFiltro);
        
    }    
    
    public String limpiarFiltro(){
        
        fechaFiltro = null;
        destinoFiltro = 0;
        placaFiltro = null;
        
        pedidos = pedidoLN.getTodosPedidos();
        
        return "viewPedidos.xhtml";
    }
    
    public void tipoCargaCambio(){
        if(tipoCarga != 0){
            log.info("Cargando placas dinamicamente...");
            carrosPorCarga = carroLG.getCarroPorCarga(tipoCarga);
//            log.info(carrosPorCarga);
        }
    }
    
    public void destinoCambio(){
//        log.info(pedido.getDestino().getId());
        if(idDestino != 0){
            log.info("Cargando placas dinamicamente...");
            carrosPorDestino = carroLG.getCarroDestino(idDestino);
//            log.info(carrosPorDestino);
        }
    }
    
    public void handleToggle(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public List<Pedido> getPedidos() {
        log.info("Listando pedidos...");
        if(pedidos == null){
            pedidos = new ArrayList<>();
            pedidos = pedidoLN.getTodosPedidos();
        }        
        
//        log.info(pedidos);
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }
    
    public List<Carro> getCarros(){
        if(carros == null){
            
            log.info("Obteniendo la lista de Carros!!!!");
            carros = carroLG.getCarros();
//            log.info(carros);
        }
        return carros;
    }
    
    public List<Destino> getDestinos(){        
        if(destinos == null){
           log.info("¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡Obtener la lista destinos!!!");
            destinos = destinoLG.getDestinos();
        }
        
        return destinos;
    }

    public Pedido getPedido() {
        if(pedido == null){
            pedido = new Pedido();
            pedido.setDestino(new Destino());           
        }
        
        return pedido;
    }
    
    public String cerrarSesion(){
        return "/index?faces-redirect=true";
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Date getFechaFiltro() {
        return fechaFiltro;
    }

    public void setFechaFiltro(Date fechaFiltro) {
        this.fechaFiltro = fechaFiltro;
    }

    public String getPlacaFiltro() {
        return placaFiltro;
    }

    public void setPlacaFiltro(String placaFiltro) {
        this.placaFiltro = placaFiltro;
    }

    public int getDestinoFiltro() {
        return destinoFiltro;
    }

    public void setDestinoFiltro(int destinoFiltro) {
        this.destinoFiltro = destinoFiltro;
    }

    public List<String> getListaDestinos() {
        return listaDestinos;
    }

    public void setListaDestinos(List<String> listaDestinos) {
        this.listaDestinos = listaDestinos;
    }

    public List<String> getListaCarros() {
        return listaCarros;
    }

    public void setListaCarros(List<String> listaCarros) {
        this.listaCarros = listaCarros;
    }

    public int getTipoCarga() {
        return tipoCarga;
    }

    public void setTipoCarga(int tipoCarga) {
        this.tipoCarga = tipoCarga;
    }

    public List<Carro> getCarrosPorCarga() {        
        return carrosPorCarga;
    }

    public void setCarrosPorCarga(List<Carro> carrosPorCarga) {
        this.carrosPorCarga = carrosPorCarga;
    }

    public List<Carro> getCarrosPorDestino() {
        return carrosPorDestino;
    }

    public void setCarrosPorDestino(List<Carro> carrosPorDestino) {
        this.carrosPorDestino = carrosPorDestino;
    }

    public int getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(int idDestino) {
        this.idDestino = idDestino;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    

    
    
    
    
    
    
    
    
}

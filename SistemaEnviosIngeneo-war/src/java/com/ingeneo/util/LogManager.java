/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Strangexz
 */
public class LogManager {
     /** Formato de Log de Salida */
    public static final String LOG_FMT="%d{yyyy.MM.dd HH:mm:ss.SSS} [%-10.10t] %-5p %c{1}.%M - %m%n";
    
    /** Configuracion realizada? */
    private static boolean configured=false;


    
    /**
     * Constructor Privado
     */
    private LogManager()
    {
    }
    
    public static final synchronized Logger initLogger(Class clazz)
    {
        Logger log = Logger.getLogger(clazz);
        initLog4J();
        return log;
    }
    /**
     * Inicializa el Logger
     */
    public static synchronized void initLog4J()
    {
        // Configuracion ya realizada?
        if (configured) return;

        // Archivo default para cargar configuracion
        File logConf= new File("log4jWAR.properties");

        // Si archivo existe cargamos configuracion
        if (logConf.exists())
        {
            System.out.println("initLog4J: ARCHIVO["+logConf.getAbsolutePath()+"] encontrado!, cargando configuración...");
            PropertyConfigurator.configure(logConf.getAbsolutePath());
            System.out.println("initLog4J: Configuración Éxitosa!!!");
            configured=true;
        }
        else // Si no existe creamos configuracion default
        {

            System.out.println("initLog4J: ARCHIVO["+logConf.getAbsolutePath()+"] no encontrado encontrado! usando configuración por defecto...");
            try
            {
                PrintWriter out= new PrintWriter(logConf);
                out.println("###########################################################");
                out.println("# Log4jUtils - Configuracion Default de LOG4J");
                out.println("# Archivo generado automaticamente el "+new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
                out.println("###########################################################");
                out.println();

                out.println("!------------------");
                out.println("! Patron de Salida");
                out.println("!------------------");
                out.println("LOG_PATTERN="+LOG_FMT);
                out.println();

                out.println("!---------------------------------------------------------------");
                out.println("! Establecemos Nivel de Salida al rootLogger, todos los loggers");
                out.println("! heredan este nivel, No modificarlo, para cambiar nivel de un");
                out.println("! paquete especifico, mejor agregarlo al final del archivo asi:");
                out.println("! log4j.logger.<paquete>=<TRACE|DEBUG|INFO|WARN|ERROR|FATAL>");
                out.println("!---------------------------------------------------------------");
                out.println("log4j.rootLogger=INFO, A1");
                out.println();

                out.println("!------------------------------------");
                out.println("! Definimos salida default a consola");
                out.println("!------------------------------------");
                out.println("log4j.appender.A1=org.apache.log4j.ConsoleAppender");
                out.println("log4j.appender.A1.layout=org.apache.log4j.PatternLayout");
//                out.println("log4j.appender.A1.layout.ConversionPattern=${LOG_PATTERN}");
                 out.println("log4j.appender.A1.layout.ConversionPattern=%d{yyyy.MM.dd-HH:mm:ss}[%p][%-c{1}]-%m %n");
                out.println();

                out.println("!-----------------------------------------");
                out.println("! Configuracion Default de paquetes.");
                out.println("!-----------------------------------------");
                out.println("log4j.logger=INFO");
                out.println();

                out.println("!---------------------------------------");
                out.println("! Configuracion de paquetes adicionales");
                out.println("!---------------------------------------");

                out.close();
                
                // Configuramos LOG4J !
                PropertyConfigurator.configure(logConf.getAbsolutePath());
                configured=true;
            }
            catch (FileNotFoundException e)
            {
                System.out.println("initLog4J: Error guardando configuracion -> "+e);
            }

        }
    }
}
